//
//  CalculationsClass.swift
//  Voice Calculator
//
//  Created by Piotr Krzesaj on 18.06.2017.
//  Copyright © 2017 Piotr Krzesaj. All rights reserved.
//

import Foundation

class CalculationsClass {
    
    var errorMsg = ""
    
    static var cc = CalculationsClass()
    
    private let priority = ["*", "/", "+", "-"] // first is * or / and next + and -
    
    func caluculate(convertedArray:Array<String>) -> String{
        if (validateConvertedArray(convertedArray: convertedArray) == false) || (errorMsg != ""){
            print("PET: validate false")
            
            return errorMsg
        }else{
            print("PET: validate true")
            
            return calculatePriority(priority: priority, convertedArray: convertedArray)[0]
        }
        
    }
    
    
    /// validating array with numbers and operators and giving feedback
    ///
    /// - Parameter convertedArray: array converted to numbers and operators
    /// - Returns: return bool and optional errorMsg whats wrong
    private func validateConvertedArray(convertedArray:Array<String>) -> Bool{
        var returnValue = false
        
        for item in 0...convertedArray.count-1{
            
            if Int(convertedArray[item]) != nil{
                returnValue = true
                
            }else {
                print("PET: int \(Int(convertedArray[item]))")
                switch convertedArray[item]{
                case "-":
                    returnValue = true
                case "+":
                    returnValue = true
                case "*":
                    returnValue = true
                case "/":
                    returnValue = true
                default:
                    returnValue = false
                    errorMsg = "Error 400"
                    
                }
                
            }
            
            for item in 0...priority.count-1{
                if (priority[item] == convertedArray[0]) || (priority[item] == convertedArray[convertedArray.count-1]){
                    if convertedArray[0] != "-" || convertedArray[convertedArray.count-1] == priority[item]{
                        returnValue = false
                        errorMsg = "Error 401"
                    }
                }
            }
            
            
        }
        
        return returnValue
    }
    
    
    /// calculation using priority
    ///
    /// - Parameters:
    ///   - priority: array with priority operators
    ///   - convertedArray: array converted to numbers and operators
    /// - Returns: return single array[0] with result
    private func calculatePriority(priority:Array<String>, convertedArray:Array<String>) -> Array<String>{
        
        var calculationArray = convertedArray
        
        for priorityCounter in 0...priority.count-1{
            
            var item = 0
            while item < calculationArray.count {
                
                if priority[priorityCounter] == calculationArray[item]{
                    var result:Any = ""
                    var specialCalc = false
                    
                    switch priority[priorityCounter]{
                    case "+":
                        if (item > 1) && (calculationArray[item-2] == "-"){
                            result = addition(item1: 0 - Int(calculationArray[item-1])!, item2: Int(calculationArray[item+1])!)
                            specialCalc = true
                        }else{
                            result = addition(item1: Int(calculationArray[item-1])!, item2: Int(calculationArray[item+1])!)
                        }
                    case "-":
                        if (item > 1) && (calculationArray[item-2] == "-"){
                            result = substraction(item1: 0 - Int(calculationArray[item-1])!, item2: Int(calculationArray[item+1])!)
                            specialCalc = true
                        }else{
                            result = substraction(item1: Int(calculationArray[item-1])!, item2: Int(calculationArray[item+1])!)
                        }
                    case "/":
                        if (item > 1) && (calculationArray[item-2] == "-"){
                            result = division(item1: 0 - Int(calculationArray[item-1])!, item2: Int(calculationArray[item+1])!)
                            specialCalc = true
                        }else{
                            result = division(item1: Int(calculationArray[item-1])!, item2: Int(calculationArray[item+1])!)
                        }
                    case "*":
                        if (item > 1) && (calculationArray[item-2] == "-"){
                            result = multiplication(item1: 0 - Int(calculationArray[item-1])!, item2: Int(calculationArray[item+1])!)
                            specialCalc = true
                        }else{
                            result = multiplication(item1: Int(calculationArray[item-1])!, item2: Int(calculationArray[item+1])!)
                        }
                    default:
                        print("PET: unexpected default switch in calculationClass, please add function to new functionality")
                    }
                    if String(describing: result) != ""{
                        if specialCalc == true{
                            calculationArray.remove(at: item-2)
                            calculationArray.remove(at: item-2)
                            calculationArray.remove(at: item-2)
                            calculationArray.remove(at: item-2)
                            
                            calculationArray.insert(String(describing: result), at: item-2)
                            item = 0
                        }else{
                            calculationArray.remove(at: item-1)
                            calculationArray.remove(at: item-1)
                            calculationArray.remove(at: item-1)
                            
                            calculationArray.insert(String(describing: result), at: item-1)
                            item = 0
                        }
                    }
                    
                    
                }
                item += 1
                
            }
            
            
            
        }
        
        print("PET: Result is: \(calculationArray)")
        return calculationArray
    }
    
    private func addition(item1:Int, item2:Int) -> Int{
        return item1 + item2
    }
    private func substraction(item1:Int, item2:Int) -> Int{
        return item1 - item2
    }
    private func multiplication(item1:Int, item2:Int) -> Int{
        return item1 * item2
    }
    private func division(item1:Int, item2:Int) -> Float{
        return Float(item1) / Float(item2)
    }
    

    
    
    
    

}
