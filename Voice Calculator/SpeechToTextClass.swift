//
//  SpeechToTextClass.swift
//  Voice Calculator
//
//  Created by Piotr Krzesaj on 18.06.2017.
//  Copyright © 2017 Piotr Krzesaj. All rights reserved.
//

import Foundation

class SpeechToTextClass {
    
    static let sttc = SpeechToTextClass()
    
    
    func speechToArray(speechText:String) -> Array<String> {
        var speechInArray:Array<String>
        
        speechInArray = speechText.lowercased().components(separatedBy: " ")
        
        print("PET: \(speechInArray)")
        return speechInArray
    }
    
    
}

