//
//  convertToMath.swift
//  Voice Calculator
//
//  Created by Piotr Krzesaj on 18.06.2017.
//  Copyright © 2017 Piotr Krzesaj. All rights reserved.
//

import Foundation

class ConvertToMath {
    
    static var ctm = ConvertToMath()
    
    private let dictionary = ["minus":"-", "plus":"+","dodać":"+","odjąć":"-", "razy":"*","przez":"/","dzielone przez":"/"]
    private let dictionary2 = ["jeden":"1", "dwa":"2", "trzy":"3", "cztery":"4", "pięć":"5", "sześć":"6", "siedem":"7", "osiem":"8", "dziewięć":"9"]
    
    /// converting speech to machine understanding array
    ///
    /// - Parameter arrayToConvert: array from textfield to convert
    /// - Returns: returning converted array to numbers and operators
    func convertViaDictionary(arrayToConvert:Array<String>) -> Array<String>{
        
        var arrayToConvert2 = arrayToConvert
        
        var arrayConverted:Array<String> = [""]
        arrayConverted.removeAll()
        
        var item = 0
        while item < arrayToConvert2.count {
            
            if (arrayToConvert2[item].characters.first == "-") && (arrayToConvert2[item].characters.count > 1){
                arrayToConvert2[item] = String(arrayToConvert2[item].characters.dropFirst(1))
                arrayToConvert2.insert("-", at: item)
                item = 0
            }
            
            if dictionary[arrayToConvert2[item]] != nil{
                
                arrayConverted.append(dictionary[arrayToConvert2[item]]!)
                
            }else if dictionary2[arrayToConvert2[item]] != nil{
                
                arrayConverted.append(dictionary2[arrayToConvert2[item]]!)
                
                
            }else if arrayToConvert2[item] == " "{
                
                
            }else{
                if arrayToConvert2[item] != nil{
                    arrayConverted.append(arrayToConvert2[item])
                }
            }
            
            item += 1
        }
        
        
        print("PET: converted \(arrayConverted)")
        return arrayConverted
    }
    
    
    

}
