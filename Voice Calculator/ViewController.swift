//
//  ViewController.swift
//  Voice Calculator
//
//  Created by Piotr Krzesaj on 18.06.2017.
//  Copyright © 2017 Piotr Krzesaj. All rights reserved.
//

import UIKit
import Speech

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var historyOfResults = [""]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var calcTextField: UITextField!
    @IBAction func calcEndEdittingAction(_ sender: Any) {
        
        
        
    }
    @IBAction func valueChangedCalcAction(_ sender: Any) {
        
        
        
        
    }
    
    @IBOutlet weak var resultLabel: UILabel!
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let historyResults = UserDefaults.standard.object(forKey: "historyResults"){
            historyOfResults = historyResults as! Array<String>
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyOfResults.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        
        cell.textLabel?.text = historyOfResults[indexPath.row]
        
        return cell
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        calcTextField.resignFirstResponder()
        return true
    }
//
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//
//            historyOfResults.remove(at: indexPath.row)
//            UserDefaults.standard.set(historyOfResults, forKey: "historyResults")
//            tableView.reloadData()
//
//        }
//    }
//    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.loadResult()
        })
        
        return true
    }
    
    
    
    func loadResult(){
        print("PET: \(String(describing: calcTextField.text))")
        self.view.resignFirstResponder()
        tableView.reloadData()
        
        if calcTextField.text != "" && calcTextField.text != nil{
            let speechToText = SpeechToTextClass.sttc.speechToArray(speechText: calcTextField.text!)
            let convertToMath = ConvertToMath.ctm.convertViaDictionary(arrayToConvert: speechToText)
            
            let result = CalculationsClass.cc.caluculate(convertedArray: convertToMath)
            
            if errorAlert(errorMsg: result) == false{
                resultLabel.text = "Wynik: \(result)"
                addNewResult(result: "\(calcTextField.text!) = \(result)")
                calcTextField.text = ""
                
            }
            
            
        }
        
        
    }
    
    func addNewResult(result:String) -> Void{
        historyOfResults.insert(result, at: 0)
        UserDefaults.standard.set(historyOfResults, forKey: "historyResults")
    }
    
    
    func errorAlert(errorMsg:String) -> Bool{
        switch errorMsg {
        case "Error 400":
            msgForUser(msg: "Nie jestem wstanie policzyć tego działania, obsługuje tylko +, -, *, /.")
            return true
        case "Error 401":
            msgForUser(msg: "Znak działania na początku lub na końcu uniemożliwia mi policzenie działania.")
            return true
        default:
            return false
        }
        
        
    }
    
    func msgForUser(msg:String) -> Void{
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        calcTextField.text = ""
        
    }
    
    
    

}

